<?php
namespace clases;

class Numeros { 
    private $numeros=[];
    private $labels=[
    ];
    
    function __construct($ns) { //para instanciar esta clase se pide dos numeros en un array de dos
        $this->setNumeros($ns);
        $this->setLabels([
            'número1',
            'número2',
        ]);
    }
    
    public function labels(){
        
        $this->labels=['numero1','numero2'];
        
    }
    
    function getLabels() {
        return $this->labels;
    }
   
    function setLabels($labels): void {
        $this->labels = $labels;
    }

        
    
    function getNumeros() {
        return $this->numeros;
    }

    function setNumeros($numeros): void {
        $this->numeros = $numeros;
    }
    
    function sumaNumeros (){
        if(!empty($this->getNumeros())){
            return $this->getNumeros()[0]+$this->getNumeros()[1];        
        }else{
            return '';
        }
    }
    
    
    
}
